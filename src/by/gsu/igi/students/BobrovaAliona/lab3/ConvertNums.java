package by.gsu.igi.students.BobrovaAliona.lab3;

import com.sun.xml.internal.messaging.saaj.util.CharReader;
import sun.invoke.empty.Empty;

import javax.xml.bind.SchemaOutputResolver;
import java.nio.CharBuffer;
import java.util.Scanner;

public class ConvertNums {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in); //Стандартный поток ввода (клавиатура) представлен объектом — System.in.
        float speed = 0;
        int units = 0;  // ед. изм
        boolean isTrue = false;

        while (isTrue == false) {
            System.out.println("Enter units (1-kmh,2-mph,3-kn,4-ms): ");
            units = in.nextInt(); //чтобы получить введенное число с клавиатуры, используется метод in.nextInt()
            if (units >= 1 && units <= 4) {
                isTrue = true;
            } else
                System.out.println("Error!");
        }
        System.out.println("Enter speed: ");
        speed = in.nextFloat();

        if (units == 1) {
            System.out.println(speed + "kmh = " + (speed * 1000 / 3600) + "ms");

        }

        if (units == 2) {
            System.out.println(speed + "mph = " + (speed * 1.609344 * 1000 / 3600) + "ms");
        }

        if (units == 3) {
            System.out.println(speed + "kn = " + (speed * 0.51444444444) + "ms");
        }

        if (units == 4) {
            System.out.println(speed + "ms = " + speed + "ms");
        }


    }
}

package by.gsu.igi.students.BobrovaAliona.lab2;

import java.util.Scanner;

public class Round {
    public static void main(String[] args) {
        double pi = 3.1416;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter radius :");
        double radius = in.nextDouble();
        System.out.println("radius=" + radius);
        //периметр
        double perimeter;
        perimeter = (radius) * 2 * pi;
        System.out.println("The perimeter is :" + perimeter);
        //площадь
        double area;
        area = pi * (radius) * (radius);
        System.out.println("Area is :" + area);


    }
}
